# Herbstluftwm config files and useful apps
    ## Base packages : 
     - Herbstluftwm
     - picom
     - vim
        ### On Arch :
        - Install base-devel and yay
        ### On Ubuntu
        - Install
            - libxft-dev
            - libx11-dev
            - libxinerama-dev
            - build-essential